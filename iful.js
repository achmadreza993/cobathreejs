import * as THREE from 'three'

const canvas = document.querySelector('#bg')

let camSize = {
    width: window.innerWidth,
    height: window.innerHeight,
}

let scene = new THREE.Scene()
scene.background = new THREE.Color(0x000000)


let cam = new THREE.PerspectiveCamera(75,camSize.width/camSize.height, 1, 1000)
let newCam = new THREE.PerspectiveCamera(75,camSize.width/camSize.height, 1, 1000)
// let newCam = new THREE.PerspectiveCamera()
let camHelper = new THREE.CameraHelper(newCam)
scene.add(camHelper)

let renderer = new THREE.WebGLRenderer({
    canvas:canvas
})

let light = new THREE.AmbientLight()
let textureLoader = new THREE.TextureLoader()

let boxGeo = new THREE.BoxGeometry(2,2,2)
let boxMat = new THREE.MeshBasicMaterial({map: textureLoader.load('download.jpg')})
let box = new THREE.Mesh(boxGeo,boxMat)
cam.position.z = 0
// scene.add(light)
scene.add(box)
renderer.setSize(camSize.width,camSize.height)

window.addEventListener('keydown',(e)=>{
    if ("ws".includes(e.key)) {
        cam.position.z += e.key == "w"? 0.2: -0.2
    } 
    if ("ad".includes(e.key)) {
        cam.rotation.x += e.key == "a"? 0.2: -0.2
        // box.rotation.x += 0.01
    }
})

function animate() {
    requestAnimationFrame(animate)
    // box.rotation.y += 0.01
    renderer.render(scene,cam)
    // console.log(cam.position);
}

animate()
