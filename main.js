import './style.css'

import * as THREE from "three"
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

const scene = new THREE.Scene()
const camera = new THREE.PerspectiveCamera(75,window.innerWidth/window.innerHeight,0.1,1000)
const renderer = new THREE.WebGLRenderer({canvas:document.querySelector("#bg"),antialias:true})
renderer.setPixelRatio(window.devicePixelRatio)
renderer.setSize(window.innerWidth,window.innerHeight)
camera.position.setZ(40)
camera.position.setY(10)

const light = new THREE.AmbientLight( 0x404040,100 );
scene.add( light );

const loader = new GLTFLoader();
loader.load( '/asset/spartan/scene.gltf', function ( gltf ) {
    gltf.scene.children[0].scale.set(10,10,10)
	scene.add( gltf.scene );
    renderer.render(scene,camera)
}, undefined, function ( error ) {
	console.error( error );
} );

const controls = new OrbitControls( camera, renderer.domElement );
controls.enableDamping = true
camera.position.set( 0, 20, 100 );
controls.update();
function animate(){
    requestAnimationFrame(animate)
    controls.update();
    renderer.render(scene,camera)
}
animate()